from django.shortcuts import render
import json 
import pywapi
  
  
def index(request): 
    
    if request.method == 'POST': 
        city = request.POST['city'] 
        location = pywapi.get_location_ids(city)

        if location == "":
            print("Cant find")
        
        else:

            for look in location:
                location_id = look
            
            weather = pywapi.get_weather_from_weather_com(location_id)
            forecast = weather['forecasts']
            dates= []
            hightemps = []
            lowtemps = []
            
            for forecasts in forecast:
                date = forecasts['date']
                high = forecasts['high']
                low = forecasts['low']
                dates.append(date)
                hightemps.append(high)
                lowtemps.append(low)
            
            data = {
                "city": city,
                "date": dates, 
                "max": hightemps, 
                "min": lowtemps,  
                } 
            print(data) 
            
    else: 
        data ={}

    return render(request, "main/index.html", data)
    